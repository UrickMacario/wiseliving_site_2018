import './scss/app.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleDown, faPhone, faTimesCircle, faTimes, faEnvelope, faBars } from '@fortawesome/free-solid-svg-icons';
import { faInstagram, faFacebookF } from '@fortawesome/free-brands-svg-icons';

import Home from './components/Home';
import ProjectView from './components/project/ProjectView';
import ApartmentsView from './components/apartments/ApartmentsView';
import TowersView from './components/towers/TowersView';
import GalleryView from './components/gallery/GalleryView';
import LocationView from './components/location/LocationView';
import ScrollToTop from './components/ScrollToTop';
import PrivacyView from './components/privacy/PrivacyView';
import FaqsView from './components/faqs/FaqsView';
import Error from './components/404/ErrorView';
import ContactView from './components/contact/ContactView';

library.add(faChevronCircleDown, faInstagram, faFacebookF, faPhone, faTimesCircle, faTimes, faEnvelope, faBars);

ReactDOM.render(
    <Router>
        <ScrollToTop>
            <Switch>
                <Route path="/contacto" component={ContactView} />
                <Route path="/preguntas-frecuentes" component={FaqsView} />
                <Route path="/aviso-de-privacidad" component={PrivacyView} />
                <Route path="/ubicacion" component={LocationView} />
                <Route path="/galeria" component={GalleryView} />
                <Route path="/torres" component={TowersView} />
                <Route path="/departamentos" component={ApartmentsView} />
                <Route path="/proyecto" component={ProjectView} />
                <Route path="/" exact component={Home} />
                <Route component={Error} />
            </Switch>
        </ScrollToTop>
    </Router>,
    document.getElementById('App')
);