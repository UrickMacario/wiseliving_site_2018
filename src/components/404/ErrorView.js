import React from 'react';
import { Link } from 'react-router-dom';

export default function Error(){
  return(
    <section className="Error">
      <h1>404</h1>
      <h2>Lo sentimos, no pudimos encontrar esa página.</h2>
      <Link to="/" className="Button">Regresa a la página de Inicio</Link>
    </section>
  );
}

