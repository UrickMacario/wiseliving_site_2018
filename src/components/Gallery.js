import React, { Component } from 'react';

import GallerySingle from './GallerySingle';

import amenities from '../img/home/gallery/home_galeria_amenidades-squashed.jpg';
import interior from '../img/home/gallery/home_galeria_exterior-squashed.jpg';
import exterior from '../img/home/gallery/home_galeria_interior-squashed.jpg';

export default class Gallery extends Component{
    constructor(props){
        super(props);

        this.state = {
            galleries: [
                {
                    image: amenities,
                    text: 'Los habitantes de Wise Living Cañada podrán hacer uso de amenidades pensadas para el bienestar individual y colectivo.',
                    button: 'Amenidades únicas'
                },
                {
                    image: exterior,
                    text: 'Amplias áreas verdes, área de juegos, cancha de pádel, alberca semiolímpica y una vista inigualable a la reserva ecológica.',
                    button: 'Descubre nuestros exteriores'
                },
                {
                    image: interior,
                    text: 'Departamentos con acabados finamente seleccionados en pisos, baños, pasillos y habitaciones.',
                    button: 'Más sobre los departamentos'
                }
            ]
        };
    }

    renderSingleGallery = () => {
        const {galleries} = this.state;

        const singleGallery = galleries.map( (gallery, key) => {
            const { image, text, button } = gallery;

            return <GallerySingle key={key} image={image} text={text} button={button} />;
        } );

        return singleGallery;
    }

    render(){
        return(
            <section className="Gallery">
                <h2 className="Gallery-title">Galeria</h2>
                <div className="Gallery-container">
                    {this.renderSingleGallery()}
                </div>
            </section>
        );
    }
}