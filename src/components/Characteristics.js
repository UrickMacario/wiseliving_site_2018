import React from 'react';
import { Link } from 'react-router-dom';

import characteristicsBg from '../img/home/characteristics/home_caracteristicas-squashed.jpg';
import apartmentsBg from '../img/home/characteristics/home_deptos-squashed.jpg';

export default function Characteristics(){
    return(
        <section className="Characteristics">
            <div className="Characteristics-left">
                <h2 className="Characteristics-title">Características</h2>
                <div className="Characteristics-image" style={{ backgroundImage: `url(${characteristicsBg})` }}></div>
                <h3 className="Characteristics-subtitle">El Proyecto.</h3>
                <p className="Characteristics-text">Wise Living Cañada consiste en la construcción de dos torres con 13 pisos y un total de 156 departamentos. Su estilo arquitectónico contemporáneo y vanguardista lo hace una oportunidad única de inversión en bienes raíces.</p>
                <Link to="/proyecto" className="Button Characteristics-button">Más información</Link>
            </div>
            <div className="Characteristics-right">
                <h2 className="Characteristics-title">Departamentos</h2>
                <div className="Characteristics-image" style={{ backgroundImage: `url(${apartmentsBg})` }}></div>
                <h3 className="Characteristics-subtitle">Nuestros departamentos.</h3>
                <p className="Characteristics-text">En la Torre A contamos con los últimos departamentos en entrega inmediata de 110 m2 hasta 340 m2 de 1, 2 y 3 recámaras. Completamente equipados y con acabados especialmente seleccionados.</p>
                <p className="Characteristics-text">En la Torre B: Espacios desde 106m2 hasta 320m2 de 2 hasta 3 amplias recámaras. amplias con clósets, cocina completamente equipada, canceles en baños y pisos de madera.</p>
                <Link to ="/departamentos" className="Button Characteristics-button">Conócelos</Link>
            </div>
        </section>
    );
}