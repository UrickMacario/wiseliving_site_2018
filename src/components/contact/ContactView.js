import React, { Fragment } from 'react';

import Menu from '../Menu';
import ContactForm from '../Contact';
import Footer from '../Footer';

export default function Contact(){
  return(
    <Fragment>
      <Menu />
      <ContactForm padding="Contact--paddingTop" />
      <Footer />
    </Fragment>
  );
}