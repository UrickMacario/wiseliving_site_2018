import React from 'react';
import { Link } from 'react-router-dom';

import Map from './Map';

export default function Location(){
    return(
        <section className="Location" data-scrolltarget="welcomenext">
            <div className="Location-top">
                <div className="Location-top-left">
                    <h1 className="Location-top-title">La mejor ubicación: Juriquilla, Querétaro</h1>
                </div>
                <div className="Location-top-right">
                    <p className="Location-top-text">
                    Wise Living Cañada se encuentra ubicado en una de las zonas con mayor índice de plusvalía en la República Mexicana: <span className="TextHighlight">Juriquilla, Querétaro.</span> Este condominio de lujo se encuentra a tan sólo 15 minutos de la ciudad de <span className="TextHighlight">Querétaro</span> y cuenta con una gran cercanía a escuelas, universidades, supermercados, el Centro Comercial Antea y el Club Náutico de Juriquilla.<br/><br/>
                    Además, se trata de una zona con una amplia oferta cultural: centros de exposiciones, centros de investigación y eventos artísticos.<br/><br/>
                    Gracias a la herencia colonial, Juriquilla mezcla tradición y modernidad al ser una de las colonias con mayor índice de calidad de vida en Querétaro.<br/>Conectividad: Carretera federal Querétaro - San Luis Potosí.
                    </p>
                </div>
            </div>
            <Map />
            <Link to="/ubicacion" className="Button Location-bottom-button">Conoce los sitios interesantes cerca de WL</Link>
        </section>
    );
}