import React from 'react';
import PropTypes from 'prop-types';

export default function Slideshow(props){
    return(
        <div className="Options-banner" style={{ backgroundImage: `url(${props.optionsBg})` }}></div>
    );
}

Slideshow.propTypes = {
    optionsBg: PropTypes.string
};