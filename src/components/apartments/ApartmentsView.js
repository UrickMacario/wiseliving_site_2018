import React, { Fragment } from 'react';

import Menu from '../Menu';
import Welcome from '../Welcome';
import Options from './Options';
import Map from '../Map';
import Instagram from '../Instagram';
import Contact from '../Contact';
import Footer from '../Footer';

import welcomeBg from '../../img/apartments/wiseliving_departamentos_de_lujo_1.jpg';

export default function ApartmentsView(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} text="Una oportunidad de inversión para el bienestar de tu familia" />
            <Options />
            <Map />
            <Instagram marginModifier="Instagram--extraMarginTop"/>
            <Contact />
            <Footer />
        </Fragment>
    );
}