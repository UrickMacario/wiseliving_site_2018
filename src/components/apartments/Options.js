import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Slideshow from './Slideshow';

import optionsBg from '../../img/project/wiseliving_proyecto_1-squashed.jpg';
import render from '../../img/apartments/render.png';
import renderMap from '../../img/apartments/render_map.png';
import closeIcon from '../../img/close_icon.png';

let waitNClick;

class Options extends Component {
    constructor(props){
        super(props);

        this.state = {
            apartments: [
                {
                    introA: "Torre A - Tipo 1",
                    introB: "2 recámaras",
                    title: "Departamento tipo 1",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre A - Tipo 1",
                    introB: "2 recámaras",
                    title: "Departamento tipo 2",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 1",
                    introB: "2 recámaras",
                    title: "Departamento tipo 3",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 2",
                    introB: "2 recámaras",
                    title: "Departamento tipo 4",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 3",
                    introB: "2 recámaras",
                    title: "Departamento tipo 5",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 4",
                    introB: "2 recámaras",
                    title: "Departamento tipo 6",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 5",
                    introB: "2 recámaras",
                    title: "Departamento tipo 7",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                },
                {
                    introA: "Torre B - Tipo 6",
                    introB: "2 recámaras",
                    title: "Departamento tipo 8",
                    renderImg: render,
                    renderPlans: renderMap,
                    characteristics: ['2 recámaras (170m2)', '2.0 baños', 'estancia', 'cuarto de servicio', 'bodega']
                }]
        };
    }

    componentDidMount(){
        const params = this.props.location.search;
        const button = document.querySelector('[data-scrollto=welcomenext]');

        if(!params){
            return;
        }

        waitNClick = setTimeout( () => {
            button.click();
        }, 500 );
    }

    componentWillUnmount(){
        clearTimeout(waitNClick);
    }

    onApartmentTypeClick = e => {
        const element = e.target;
        const apartmentKey = element.dataset.apartmentkey;
        const reset = document.querySelector('.Options-selector-single-content--active');

        if(reset){
            reset.classList.remove('Options-selector-single-content--active');
        }

        element.classList.add('Options-selector-single-content--active');

        this.onSpecificsShown(apartmentKey);
    }

    onSpecificsShown = apartmentKey => {
        const modal = document.querySelector('.Options-characteristics');
        const apartment = this.state.apartments[apartmentKey];
        const { title, renderPlans, renderImg, characteristics } = apartment;
        const titleElement = document.querySelector('.Options-characteristics-title');
        const renderDiv = document.querySelector('.Options-characteristics-renderImage'); 
        const renderPlansDiv = document.querySelector('.Options-characteristics-renderMap');
        const renderListDiv = document.querySelector('.Options-characteristics-list');

        renderListDiv.innerHTML = '';
        modal.classList.remove('Options-characteristics--hidden');
        titleElement.textContent = title;
        renderDiv.style.backgroundImage = `url(${renderImg})`;
        renderPlansDiv.style.backgroundImage = `url(${renderPlans})`;

        characteristics.map( characteristic => {
            const paragraph = document.createElement('P');
            paragraph.classList.add('Options-characteristics-list-text');
            const text = document.createTextNode(characteristic);
            paragraph.appendChild(text);
            renderListDiv.appendChild(paragraph);
        } );
    }

    renderOptionsSingle = () => {
        const {apartments} = this.state;

        const rederApartments = apartments.map( (apartment, key) => {
            const {introA, introB} = apartment;

            return(
                <div className="Options-selector-single" key={key}>
                    <div className="Options-selector-single-content" onClick={this.onApartmentTypeClick} data-apartmentkey={key}>
                        <p className="Options-selector-single-type">{introA}</p>
                        <p className="Options-selector-single-bedrroms">Ver detalles</p>
                    </div>
                </div>
            );
        } );

        return rederApartments;   
    }

    onCloseOptionsCharacteristics = () => {
        const modal = document.querySelector('.Options-characteristics');

        modal.classList.add('Options-characteristics--hidden');
    }

    onContactClick = () => {
        const menuContactButton = document.querySelector('.Menu-nav-option--contact');
        const closeCharacteristics = document.querySelector('.Options-characteristics--close');

        menuContactButton.click();
        closeCharacteristics.click();
    }

    render(){
        return(
            <section className="Options" data-scrolltarget="welcomenext">
                <div className="Options-container">
                    <h2 className="Options-title">¿Qué departamento te gustaría visualizar?</h2>
                    <p className="Options-text">Tipo de departamentos</p>
                    <div className="Options-selector">
                        {this.renderOptionsSingle()}
                    </div>
                </div>
                <div className="Options-characteristics Options-characteristics--hidden">
                    <div className="Options-characteristics--close" style={{ backgroundImage: `url(${closeIcon})` }} onClick={this.onCloseOptionsCharacteristics}></div>
                    <h2 className="Options-characteristics-title">Departamento Tipo 1</h2>
                    <div className="Options-characteristics-render">
                        <div className="Options-characteristics-renderImage" style={{ backgroundImage: `url(${render})` }}></div>
                        <div className="Options-characteristics-renderMap" style={{ backgroundImage: `url(${renderMap})` }}></div>
                    </div>
                    <div className="Options-characteristics-list">
                    </div>
                    <Link className="Button Options-characteristics-button" onClick={this.onContactClick} to="/contacto">Contactar a un asesor</Link>
                </div>
                <Slideshow optionsBg={optionsBg} />
            </section>
        );
    }
}

Options.propTypes = {
    search: PropTypes.string,
    location: PropTypes.object
};

export default withRouter(Options);