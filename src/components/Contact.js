import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReCAPTCHA from 'react-google-recaptcha';

import DatePicker from './DatePicker';

export default class Contact extends Component {
    constructor(props){
        super(props);

        this.state = {
            fullName: '',
            mail: '',
            phone: '',
            message: '',
            date: '' 
        };
    }

    onInputChange = (e) => {
        const element = e.target;
        const elementName = element.getAttribute('name');
        const {value} = element;

        element.classList.remove('Contact-input--error');

        switch(elementName){
            case 'fullName':
            this.setState({ fullName: value });
            break;

            case 'mail':
            this.setState({ mail: value });
            break;
            
            case 'phone':
            this.setState({ phone: value });
            break;

            case 'message':
            this.setState({ message: value });
            break;
        }
    }

    onDateSelected = (date) => {
        this.setState({date: date});
    }

    onFormSubmit = (e) => {
        const { fullName, mail, phone, message } = this.state;
        e.preventDefault();

        if(!fullName || !/^[A-Za-z\s]+$/.test(fullName)){
            document.querySelector(`[name=fullName]`).classList.add('Contact-input--error');
            return;
        }

        if(!mail || !mail.includes('@') || !mail.includes('.') ){
            document.querySelector(`[name=mail]`).classList.add('Contact-input--error');
            return;
        }

        if(!phone || !/^[0-9]+$/.test(phone)){
            document.querySelector(`[name=phone]`).classList.add('Contact-input--error');
            return;
        }

        if(!message || !/^[A-Za-z0-9\s]+$/.test(message)){
            document.querySelector(`[name=message]`).classList.add('Contact-input--error');
            return;
        }
    }

    onScheduleAppointment = () => {
        const element = document.querySelector('.Contact .react-datepicker__input-container');
        element.classList.toggle('react-datepicker__input-container--activated');
    }

    onRecaptcha = () => {
        // console.log('recaptcha');
    }

    render(){
        return(
            <section className={`Contact ${this.props.padding}`} data-scrolltarget="contact">
                <h2 className="Contact-title">Agenda tu cita para conocer a Wise Living Cañada</h2>
                <form action="#" className="Contact-form" onSubmit={this.onFormSubmit}>
                    <input type="text" className="Contact-input Contact-input--full" placeholder="Nombre Completo" value={this.state.fullName} onChange={this.onInputChange} name="fullName" />
                    <div className="Contact-form-group">
                        <input type="text" className="Contact-input Contact-input-half" placeholder="Mail" value={this.state.mail} name="mail" onChange={this.onInputChange} />
                        <button className="Button Contact-form-schedule" onClick={this.onScheduleAppointment}>Agendar tu cita</button>
                        <input type="text" className="Contact-input Contact-input-half" placeholder="Teléfono" value={this.state.phone} name="phone" onChange={this.onInputChange} />
                        <DatePicker customClass="Contact-input Contact-datepicker" onDateSelected={this.onDateSelected} dropdownMode="select" fixedHeight={true} />
                    </div>
                    <textarea className="Contact-input Contact-input-area" placeholder="Mensaje" value={this.state.message} name="message" onChange={this.onInputChange}></textarea>
                    <ReCAPTCHA sitekey="6Ld6JVQUAAAAAHafmP-RCQwVN0GBiUz36FgblSFb" onChange={this.onRecaptcha} size="normal" />
                    <div className="Contact-form-submit">
                        <button className="Button Contact-form-submitButton">Enviar formulario</button>
                    </div>
                </form>
            </section>
        );
    }
}

Contact.propTypes = {
    padding: PropTypes.string
};