import React, { Fragment } from 'react';

import Menu from '../Menu';
import Footer from '../Footer';
import FaqsText from './FaqsText';

export default function FaqsView(){
  return(
    <Fragment>
      <Menu />
      <FaqsText />
      <Footer />
    </Fragment>
  );
}