import React from 'react';

export default function FaqsText(){
  return(
    <section className="FaqsText">
      <h2 className="FaqsText-title">Preguntas Frecuentes</h2>
      <p className="FaqsText-question">Are you a morning person or a night owl?</p>
      <p className="FaqsText-answer">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas architecto, enim laborum harum tempora impedit optio voluptas quo temporibus obcaecati, numquam, assumenda magni! Ipsam quae consectetur provident quia accusamus earum!</p>
    </section>
  );
}