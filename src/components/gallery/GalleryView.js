import React, { Fragment } from 'react';

import Menu from '../Menu';
import Welcome from '../Welcome';
import Options from './Options';
import Map from '../Map';
import Instagram from '../Instagram';
import Contact from '../Contact';
import Footer from '../Footer';

import welcomeBg from '../../img/gallery/wiseliving_galeria-squashed.jpg';

export default function GalleryView(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} text="Un proyecto integral para todos sus habitantes" />
            <Options />
            <Map />
            <Instagram  marginModifier="Instagram--extraMarginTop" />
            <Contact />
            <Footer />
        </Fragment>
    );
}