import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';

import Slideshow from '../Slideshow';

import exterior1 from '../../img/gallery/exterior/wiseliving_amenidades_exterior_1-squashed.jpg';
import exterior2 from '../../img/gallery/exterior/wiseliving_amenidades_exterior_2-squashed.jpg';
import exterior3 from '../../img/gallery/exterior/wiseliving_amenidades_exterior_3-squashed.jpg';
import exterior4 from '../../img/gallery/exterior/wiseliving_amenidades_exterior_4-squashed.jpg';
import interior1 from '../../img/gallery/interior/wiseliving_amenidades_interior_1-squashed.jpg';
import interior2 from '../../img/gallery/interior/wiseliving_amenidades_interior_2-squashed.jpg';
import interior3 from '../../img/gallery/interior/wiseliving_amenidades_interior_3-squashed.jpg';
import interior4 from '../../img/gallery/interior/wiseliving_amenidades_interior_4-squashed.jpg';
import amenities1 from '../../img/gallery/amenities/wiseliving_amenidades_1-squashed.jpg';
import amenities2 from '../../img/gallery/amenities/wiseliving_amenidades_2-squashed.jpg';
import amenities3 from '../../img/gallery/amenities/wiseliving_amenidades_3-squashed.jpg';
import amenities4 from '../../img/gallery/amenities/wiseliving_amenidades_4-squashed.jpg';
import amenities5 from '../../img/gallery/amenities/wiseliving_amenidades_5-squashed.jpg';
import amenities6 from '../../img/gallery/amenities/wiseliving_amenidades_6-squashed.jpg';
import amenities7 from '../../img/gallery/amenities/wiseliving_amenidades_7-squashed.jpg';

let delay;

class Options extends Component {
    constructor(props){
        super(props);

        this.state = {
            galleries: [
                [exterior1, exterior2, exterior3, exterior4],
                [interior1, interior2, interior3, interior4],
                [amenities1, amenities2, amenities3, amenities4, amenities5, amenities6, amenities7]
            ],
            currentGalleryIndex: 0
        };
    }

    componentDidMount(){
        const params = this.props.location.search;

        if(!params){
            return;
        }

        this.waitNScroll(params);
    }

    waitNScroll = section => {
        const menuHeight = document.querySelector('.Menu').offsetHeight;
        const target = document.querySelector('.GalleryOptions');
        const targetOffset = target.offsetTop;
        let option;

        delay = setTimeout( () => {
            window.scroll({
                top: targetOffset - menuHeight,
                left: 0,
                behavior: 'smooth'
            });
        }, 500 );

        switch(section){
            case '?interior':
            option = document.querySelector('[data-slideshow="0"]');
            option.click();
            break;

            case '?exterior':
            option = document.querySelector('[data-slideshow="1"]');
            option.click();
            break;

            case '?amenidades':
            option = document.querySelector('[data-slideshow="2"]');
            option.click();
            break;
        }
    }

    componentWillUnmount(){
        clearTimeout(delay);
    }

    onSlideshowButtonClick = (e) => {
        const element = e.target;
        const reset = document.querySelector('.GalleryOptions-slideshow-button--selected');
        const slideshowIndex = parseInt(element.dataset.slideshow);

        if(reset){
            reset.classList.remove('GalleryOptions-slideshow-button--selected');
        }

        element.classList.add('GalleryOptions-slideshow-button--selected');
        this.setState({ currentGalleryIndex: slideshowIndex });
    }

    renderNewGallery = () => {
        const {galleries, currentGalleryIndex} = this.state;

        return <Slideshow gallery={galleries[currentGalleryIndex]} />;
    }

    render(){
        return(
            <section className="GalleryOptions" data-scrolltarget="welcomenext">
                <div className="GalleryOptions-container">
                    <h2 className="GalleryOptions-title">Wise Living Cañada: exclusivos espacios para disfrutar.</h2>
                    <p className="GalleryOptions-text">Es un nuevo concepto que crea espacios de vida, conectando y transformando tu entorno y el de toda la zona. Crea comunidades que brindan una mejor calidad de vida para ti y tu familia.</p>
                </div>
                <div className="GalleryOptions-slideshow">
                    <div className="GalleryOptions-slideshow-options">
                        <button className="Button GalleryOptions-slideshow-button GalleryOptions-slideshow-button--selected" onClick={this.onSlideshowButtonClick} data-slideshow={0}>Exteriores</button>
                        <button className="Button GalleryOptions-slideshow-button" onClick={this.onSlideshowButtonClick} data-slideshow={1}>Interiores</button>
                        <button className="Button GalleryOptions-slideshow-button" onClick={this.onSlideshowButtonClick} data-slideshow={2}>Amenidades</button>
                    </div>
                    {this.renderNewGallery()}
                </div>
                <button className="Button GalleryOptions-button">Descarga el brochure</button>
            </section>
        );
    }
}

Options.propTypes = {
    search: PropTypes.string,
    location: PropTypes.object
};

export default withRouter(Options);