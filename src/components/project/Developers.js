import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import banner from '../../img/project/developers/wiseliving_proyecto_penninsula.jpg';

let waitNClick;

class Developers extends Component {
    componentDidMount(){
        const params = this.props.location.search;
        let button;

        if(!params){
            return;
        }

        switch(params){
            case '?desarrolladores':
            button = document.querySelector('[data-scrollto=welcomenext]');
            
            waitNClick = setTimeout( () => {
                button.click();
            }, 500 );
            break;
        }
    }

    componentWillUnmount(){
        clearTimeout(waitNClick);
    }

    render(){
        return(
            <section className="Developers" data-scrolltarget="welcomenext">
                <h2 className="Developers-title">Desarrolladores</h2>
                <div className="Developers-image" style={{ backgroundImage: `url(${banner})` }}></div>
                <h4 className="Developers-subtitle">Peninsula Investments Group</h4>
                <p className="Developers-text">Wise Living Cañada fue desarrollado por Península Investments Group y Aferba.</p>
                <p className="Developers-text">Nuestro compromiso con la calidad de construcción, acabados y formalidad en tiempos de entrega forman parte de nuestra filosofía.</p>
                <a href="http://www.peninsulainvestments.com/en" target="_blank" rel="noopener noreferrer" className="Button Developers-button">Conoce Península</a>
            </section>
        );
    }
}

Developers.propTypes = {
    search: PropTypes.string,
    location: PropTypes.object
};

export default withRouter(Developers);