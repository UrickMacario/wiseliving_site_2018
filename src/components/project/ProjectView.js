import React, { Fragment } from 'react';

import Menu from '../Menu';
import Welcome from '../Welcome';
import Developers from './Developers';
import Characteristics from './Characteristics';
import Amenities from '../Amenities';
import Map from '../Map';
import Instagram from '../Instagram';
import Contact from '../Contact';
import Footer from '../Footer';

import welcomeBg from '../../img/project/wiseliving_proyecto_1-squashed.jpg';

export default function ProyectoView(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} text="Un proyecto único para invertir en Querétaro" />
            <Developers />
            <Characteristics />
            <Amenities bg="Amenities--whiteBg" />
            <Map />
            <Instagram marginModifier="Instagram--extraMarginTop"/>
            <Contact />
            <Footer />
        </Fragment>
    );
}