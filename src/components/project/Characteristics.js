import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import banner from '../../img/project/developers/wiseliving_proyecto_caracteristicas-squashed.jpg';

let delay;

class Characteristics extends Component{
    componentDidMount(){
        const params = this.props.location.search;

        if(!params){
            return;
        }

        this.waitNScroll(params);
    }

    waitNScroll = section => {
        let target, targetOffset;
        const menuHeight = document.querySelector('.Menu').offsetHeight;

        switch(section){
            case '?caracteristicas':
            target = document.querySelector('.ProjectCharacteristics');
            targetOffset = target.offsetTop;

            delay = setTimeout( () => {
                window.scroll({
                    top: targetOffset - menuHeight,
                    left: 0,
                    behavior: 'smooth'
                });
            }, 500 );

            break;
        }
    }

    componentWillUnmount(){
        clearTimeout(delay);
    }

    render(){
        return(
            <section className="ProjectCharacteristics">
                <h2 className="ProjectCharacteristics-title">Características</h2>
                <div className="ProjectCharacteristics-image" style={{ backgroundImage: `url(${banner})` }}></div>
                <h4 className="ProjectCharacteristics-subtitle">Un proyecto integral para todos sus habitantes</h4>
                <p className="ProjectCharacteristics-text">Pensando para el bienestar de los usuarios de este desarrollo inmobiliario, hemos creado un ambiente que inspira: con sus departamentos, amenidades y seguridad.</p>
                <Link to="/departamentos?scroll=options" className="Button ProjectCharacteristics-button">Conoce más</Link>
                <div className="ProjectCharacteristics-twoColumns">
                    <div className="ProjectCharacteristics-touches">
                        <h2 className="ProjectCharacteristics-title">Acabados</h2>
                        <ul className="ProjectCharacteristics-list">
                            <li className="ProjectCharacteristics-list-item">Cocina integral equipada</li>
                            <li className="ProjectCharacteristics-list-item">Recámaras con clósets terminados</li>
                            <li className="ProjectCharacteristics-list-item">Baños con canceles en regaderas</li>
                            <li className="ProjectCharacteristics-list-item">Piso de madera en recámaras</li>
                            <li className="ProjectCharacteristics-list-item">Luminarias en plafón</li>
                            <li className="ProjectCharacteristics-list-item">Calentador eléctrico</li>
                        </ul>
                    </div>
                    <div className="ProjectCharacteristics-security">
                        <h2 className="ProjectCharacteristics-title">Seguridad</h2>
                        <ul className="ProjectCharacteristics-list">
                            <li className="ProjectCharacteristics-list-item">Seguridad para ti y tu familia: este complejo cuenta con seguridad privada y vigilancia 24/7.</li>
                            <li className="ProjectCharacteristics-list-item">Sistema de seguridad con CCTV en áreas comunes, accesos y estacionamientos.</li>
                            <li className="ProjectCharacteristics-list-item">Planta de energía eléctrica para emergencias.</li>
                            <li className="ProjectCharacteristics-list-item">Rutas y salidas de emergencia.</li>
                        </ul>
                    </div>
                </div>
            </section>
        );
    }
}

Characteristics.propTypes = {
    search: PropTypes.string,
    location: PropTypes.object
};

export default withRouter(Characteristics);