import React, { Component } from 'react';
import $ from 'jquery';
import PropTypes from 'prop-types';

import chevron from '../img/next_icon.png';


let slideImages, delay;
let counter = 1;

export default class Welcome extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        slideImages = this.props.slides;
        this.switchBackground();
    }

    switchBackground = () => {
        const slideContainer = document.querySelector('.Welcome');

        delay = setInterval( () => {
            switch(counter){
                case 0:
                    slideContainer.style.backgroundImage = `url(${slideImages[0]})`;
                    counter += 1;
                    console.log(counter);
                break;

                case 1:
                    slideContainer.style.backgroundImage = `url(${slideImages[1]})`;
                    counter += 1;
                    console.log(counter);
                break;

                case 2:
                    slideContainer.style.backgroundImage = `url(${slideImages[2]})`;
                    counter += 1;
                    console.log(counter);
                break;

                case 3: 
                    slideContainer.style.backgroundImage = `url(${slideImages[3]})`;
                    counter += 1;
                    console.log(counter);
                break;

                case 4:
                    slideContainer.style.backgroundImage = `url(${slideImages[4]})`;
                    counter += 1;
                    console.log(counter);
                break;

                case 5:
                    slideContainer.style.backgroundImage = `url(${slideImages[5]})`;
                    counter = 0;
                    console.log(counter);
                break;

                default:
                slideContainer.style.backgroundImage = `url(${slideImages[0]})`;
                counter = 0;
                console.log(counter);

                break;
            }
        }, 5000 );
    }

    scrollTo = (e) => {
        const element = e.target;
        const elementTarget = $(element).data('scrollto');
        const targetOffset = $(`[data-scrolltarget=${elementTarget}]`).offset().top;
        const menuHeight = $('.Menu').height();
        
        window.scroll({
            top: targetOffset - menuHeight,
            left: 0,
            behavior: 'smooth'
        });
    }

    render(){
        return(
            <section className="Welcome" style={{ backgroundImage: `url(${this.props.image})` }}>
                <h2 className="Welcome-text">{ this.props.text ? this.props.text : "Una oportunidad de inversión para el bienestar de tu familia "}</h2>
                <div className="Welcome-icon" data-scrollto="welcomenext" onClick={this.scrollTo}>
                    <div className="Welcome-chevron" style={{ backgroundImage: `url(${chevron})` }}></div>
                </div> 
            </section>
        );
    }

}

Welcome.propTypes = {
    image: PropTypes.string,
    slides: PropTypes.array,
    text: PropTypes.string
};