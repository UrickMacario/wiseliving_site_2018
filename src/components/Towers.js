import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import TowersSingle from './TowersSingle';

import apartmentImage from '../img/home/towers/torre1.jpg';
import apartmentImage2 from '../img/home/towers/torre2.jpg';
import towerBanner from '../img/project/developers/banner.jpg';
import closeIcon from '../img/close_icon.png';

let waitNClick;

class Towers extends Component{

    constructor(props){
        super(props);

        this.state = {
            apartments: [
                {
                    image: apartmentImage,
                    banner: towerBanner,
                    title: 'Torre A: Vendida al 90%',
                    popTitle: 'Características: Torre A',
                    text: 'Con una impresionante vista a la reserva ecológica de La Cañada en Juriquilla, la primer torre de este desarrollo cuenta con departamentos en venta y renta de dos y tres recámaras.',
                    innerText: 'La primer torre de Wise Living Cañada cuenta con los siguientes departamentos disponibles para renta y venta con entrega inmediata:',
                    characteristics: ['De 61 a 182 m2', '1, 2 y 3 recámaras', 'Sala Comedor', 'Cocina', 'Área de lavado', 'De 1 a 3 lugares de estacionamiento', 'Balcones'],
                    specialCharacteristics: ['Cuarto de servicio', 'Family Room', 'Bodega'],
                    onSale: ['Bodega', 'Cajones de estacionamiento'],
                    finishingTouches: ['Cocina integral equipada con cubierta de cuarto', 'Recámaras con closets terminados', 'Baños con canceles en regaderas', 'Piso de madera de ingeniería en recámaras', 'Piso de porcelanato en sala, comedor, cocina y baños', 'Luminarias en plafón con focos ahorradores', 'Calentador de paso para gas natural'],
                    security: ['Seguridad privada y vigilancia las 24 horas, 7 días de la semana.', 'Sistema de seguridad con CCTV en: áreas comunes, accesos y estacionamiento.', 'Sistema de detección de humo en áreas privativas y en áreas comunes.', 'Planta de energía eléctrica para emergencias.']
                },
                {
                    image: apartmentImage2,
                    banner: towerBanner,
                    popTitle: 'Características: Torre B',
                    title: 'Torre B: Preventa',
                    text: 'La construcción de esta torre comenzará en 2018 y se encuentra en fase de preventa. Desde 114.16 m2.',
                    characteristics: ['De 61 a 182 m2', '1, 2 y 3 recámaras', 'Sala Comedor', 'Cocina', 'Área de lavado', 'De 1 a 3 lugares de estacionamiento', 'Balcones'],
                    specialCharacteristics: ['Cuarto de servicio', 'Family Room', 'Bodega'],
                    onSale: ['Bodega', 'Cajones de estacionamiento'],
                    finishingTouches: ['Cocina integral equipada con cubierta de cuarto', 'Recámaras con closets terminados', 'Baños con canceles en regaderas', 'Piso de madera de ingeniería en recámaras', 'Piso de porcelanato en sala, comedor, cocina y baños', 'Luminarias en plafón con focos ahorradores', 'Calentador de paso para gas natural'],
                    security: ['Seguridad privada y vigilancia las 24 horas, 7 días de la semana.', 'Sistema de seguridad con CCTV en: áreas comunes, accesos y estacionamiento.', 'Sistema de detección de humo en áreas privativas y en áreas comunes.', 'Planta de energía eléctrica para emergencias.']
                }
            ]
        };
    }

    renderSingleApartment = () => {
        const apartments = this.state.apartments;
        const singleApartment = apartments.map( (apartment, key) => {
            const { image, title, text } = apartment;

            return <TowersSingle key={key} image={image} title={title} text={text} tower={key} onTowerSingleDetails={this.onTowerSingleDetails} buttonText="Conoce las torres" />;
        } );

        return singleApartment;
    }

    renderScrollTarget = () => {
        const location = this.props.location.pathname;
        let data = '';

        switch(location){
            case '/torres':
            data = 'welcomenext';
            break;
        }

        return data;
    }

    onOpenTowerSingleDetails = () => {
        const element = document.querySelector('.ProjectCharacteristics--tower');
        element.classList.add('ProjectCharacteristics--tower--active');
    }

    onTowerSingleDetails = (e) => {
        this.onOpenTowerSingleDetails();

        const apartmentIndex = e.target.dataset.apartment;
        const apartment = this.state.apartments[apartmentIndex];
        const { banner, text, characteristics, specialCharacteristics, onSale, finishingTouches, security, popTitle } = apartment;

        const bannerElement = document.querySelector('.ProjectCharacteristics-image');
        const textElement = document.querySelector('.ProjectCharacteristics-text');
        const noBullets = document.querySelector('.ProjectCharacteristics-list--left');
        const specialList = document.querySelector('.ProjectCharacteristics-list-special');
        const saleList = document.querySelector('.ProjectCharacteristics-list-sale');
        const finishingTouchesList = document.querySelector('.ProjectCharacteristics-touches .ProjectCharacteristics-list');
        const securityList = document.querySelector('.ProjectCharacteristics-security .ProjectCharacteristics-list');
        const title = document.querySelector('.ProjectCharacteristics-title');

        noBullets.innerHTML = '';
        specialList.innerHTML = '<li class="ProjectCharacteristics-list-item ProjectCharacteristics-list-item--noBullet">Algunos departamentos cuentan con:</li>';
        saleList.innerHTML = '<li class="ProjectCharacteristics-list-item ProjectCharacteristics-list-item--noBullet">En venta:</li>';
        finishingTouchesList.innerHTML = '';
        securityList.innerHTML = '';
        bannerElement.style.backgroundImage = `url(${banner})`;
        textElement.textContent = text;
        title.innerHTML = popTitle;

        characteristics.map( characteristic => {
            const item = document.createElement('LI');
            item.textContent = characteristic;
            item.classList.add('ProjectCharacteristics-list-item');
            noBullets.appendChild(item);            
        } );

        specialCharacteristics.map( special => {
            const item = document.createElement('LI');
            item.textContent = special;
            item.classList.add('ProjectCharacteristics-list-item');
            specialList.appendChild(item);
        } );

        onSale.map( sale => {
            const item = document.createElement('LI');
            item.textContent = sale;
            item.classList.add('ProjectCharacteristics-list-item');
            saleList.appendChild(item);
        } );

        finishingTouches.map( touch => {
            const item = document.createElement('LI');
            item.textContent = touch;
            item.classList.add('ProjectCharacteristics-list-item');
            finishingTouchesList.appendChild(item);
        } );

        security.map( sec => {
            const item = document.createElement('LI');
            item.textContent = sec;
            item.classList.add('ProjectCharacteristics-list-item');
            securityList.appendChild(item);
        } );

    }

    onCloseProjectCharacteristics = () => {
        const element = document.querySelector('.ProjectCharacteristics--tower');
        element.classList.remove('ProjectCharacteristics--tower--active');
    }

    componentDidMount(){
        const params = this.props.location.search;
        const button = document.querySelector('[data-scrollto=welcomenext]');

        if(!params){
            return;
        }

        waitNClick = setTimeout( () => {
            button.click();
        }, 500 );
    }

    componentWillUnmount(){
        clearTimeout(waitNClick);
    }

    render(){
        return(
            <section className={`Towers ${this.props.bg}`} data-scrolltarget={this.renderScrollTarget()}>
                <div className="Towers-container">
                    <h2 className="Towers-title">Conoce nuestras torres</h2>
                    <h3 className="Towers-subtitle">Wise Living Cañada propone un concepto de vivienda integral al ubicarse en un vecindario con alta plusvalía y al contar con amenidades únicas en la zona.</h3>
                    <div className="Towers-show">
                        {this.renderSingleApartment()}
                    </div>
                </div>
                <div className="ProjectCharacteristics ProjectCharacteristics--tower">
                    <div className="ProjectCharacteristics--tower--close" onClick={this.onCloseProjectCharacteristics} style={{ backgroundImage: `url(${closeIcon})` }}></div>
                    <h2 className="ProjectCharacteristics-title">Características</h2>
                    <div className="ProjectCharacteristics-image"></div>
                    <p className="ProjectCharacteristics-text"></p>
                    <div className="ProjectCharacteristics-twoColumns">
                        <ul className="ProjectCharacteristics-list--left"></ul>
                        <div className="ProjectCharacteristics-list--right">
                            <ul className="ProjectCharacteristics-list-special"></ul>
                            <ul className="ProjectCharacteristics-list-sale"></ul>
                        </div>
                    </div>
                    <div className="ProjectCharacteristics-twoColumns">
                        <div className="ProjectCharacteristics-touches">
                            <h2 className="ProjectCharacteristics-title">Acabados</h2>
                            <ul className="ProjectCharacteristics-list"></ul>
                        </div>
                        <div className="ProjectCharacteristics-security">
                            <h2 className="ProjectCharacteristics-title">Seguridad</h2>
                            <ul className="ProjectCharacteristics-list"></ul>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

Towers.propTypes = {
    bg: PropTypes.string,
    location: PropTypes.object,
    pathname: PropTypes.string
};

const TowersWithRouter = withRouter(Towers);

export default TowersWithRouter;