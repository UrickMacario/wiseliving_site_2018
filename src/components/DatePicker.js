import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import PropTypes from 'prop-types';

import 'react-datepicker/dist/react-datepicker-cssmodules.css';

export default class ContactDatePicker extends Component {
  constructor(props){
    super(props);

    this.state = {
      startDate: moment()
    };
  }

  onDateChange = (date) => {
    this.setState({
      startDate: date
    });

    this.props.onDateSelected(date.format("MMMM / DD / YYYY - HH:mm"));
  }

  render(){
    return(
      <DatePicker className={this.props.customClass} selected={this.state.startDate} onChange={this.onDateChange} name="date" dateFormat="LLL" showMonthYearDropdown withPortal showTimeSelect  />
    );
  }
}

ContactDatePicker.propTypes = {
  customClass: PropTypes.string,
  onDateSelected: PropTypes.func
};