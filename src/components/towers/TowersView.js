import React, { Fragment } from 'react';

import Menu from '../Menu';
import Welcome from '../Welcome';
import Towers from '../Towers';
import Slideshow from '../apartments/Slideshow';
import Amenities from '../Amenities';
import Map from '../Map';
import Instagram from '../Instagram';
import Contact from '../Contact';
import Footer from '../Footer';

import welcomeBg from '../../img/tower/wiseliving_torres_1-squashed.jpg';
import slideshowBg from '../../img/apartments/banner.jpg';

export default function TowersView(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} text="Dos torres de 13 pisos para invertir en bienes raíces" />
            <Towers bg="Towers--whiteBg" />
            <Slideshow optionsBg={slideshowBg} />
            <Amenities bg="Amenities--whiteBg" />
            <Map />
            <Instagram marginModifier="Instagram--extraMarginTop" />
            <Contact />
            <Footer />
        </Fragment>
    );
}