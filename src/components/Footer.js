import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default function Footer(){
    return(
        <footer className="Footer">
            <div className="Footer-social">
                <a href="https://www.facebook.com/wiselivingcanada/" target="_blank" rel="noopener noreferrer">
                    <FontAwesomeIcon icon={['fab', 'facebook-f']} />
                </a>
                <a href="https://www.instagram.com/wiselivingcanada/" target="_blank" rel="noopener noreferrer">
                    <FontAwesomeIcon icon={['fab', 'instagram']} />
                </a>
                <a href="tel:+524422946060" target="_blank" rel="noopener noreferrer">
                    <FontAwesomeIcon icon="phone" />
                </a>
            </div>
            <ul className="Footer-list">
                <li className="Footer-list-item"><a href="tel:+524422946060" target="_blank" rel="noopener noreferrer">(442) 294 6060</a></li>
                <li className="Footer-list-item"><Link to="/aviso-de-privacidad">Aviso de privacidad</Link></li>
                <li className="Footer-list-item"><Link to="/preguntas-frecuentes">Preguntas frecuentes</Link></li>
            </ul>
            <p className="Footer-mail"><a href="mailto:contacto@wiseliving.com.mx" target="_blank" rel="noopener noreferrer">contacto@wiseliving.com.mx</a></p>
        </footer>
    );
}