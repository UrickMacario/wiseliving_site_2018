import React, { Component } from 'react';
import PropTypes from 'prop-types';

import slide1 from '../img/home/amenities/home_amenidades1-squashed.jpg';
import slide2 from '../img/home/amenities/home_amenidades2-squashed.jpg';
import slide3 from '../img/home/amenities/home_amenidades3-squashed.jpg';
import slide4 from '../img/home/amenities/amenidad2-squashed.jpg';
import slide5 from '../img/home/amenities/caracteriticas-squashed.jpg';
import slide6 from '../img/home/amenities/IMG_5433-squashed.jpg';
import slide7 from '../img/home/amenities/IMG_5446-squashed.jpg';
import slide8 from '../img/home/amenities/IMG_5461-squashed.jpg';
import slide9 from '../img/home/amenities/IMG_5486-squashed.jpg';
import slide10 from '../img/home/amenities/IMG_5508-squashed.jpg';
import slide11 from '../img/home/amenities/IMG_5522-squashed.jpg';

export default class SlideShow extends Component {
    componentDidUpdate(){
        const {gallery} = this.props;
        const slidesContainer = document.querySelector('.SlideShow-container');

        if(!gallery){
            return;
        }

        slidesContainer.innerHTML = '';

        gallery.map( slide => {
            const slideElement = document.createElement('DIV');

            slideElement.classList.add('SlideShow-single');
            slideElement.style.backgroundImage = `url(${slide})`;
            slidesContainer.appendChild(slideElement);
        } );
    }

    render(){
        return(
            <div className="SlideShow">
                <div className="SlideShow-container">
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide1})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide2})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide3})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide4})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide5})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide6})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide7})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide8})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide9})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide10})` }} ></div>
                    <div className="SlideShow-single" style={{ backgroundImage: `url(${slide11})` }} ></div>
                </div>
            </div>
        );
    }
}

SlideShow.propTypes = {
    gallery: PropTypes.array
};