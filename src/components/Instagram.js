import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import axios from 'axios';

export default class Instagram extends Component{
    constructor(props){
        super(props);

        this.state = {
            posts: []
        };
    }

    componentDidMount(){
        const ACCESS_TOKEN = '3868260192.7cdc885.d2f605654263413d8e047f7dfd3ca54c';

        axios({
            method: 'get',
            url: `https://api.instagram.com/v1/users/self/media/recent/?access_token=${ACCESS_TOKEN}&count=8`,
        }).then( res => {
            this.setState({ posts: res.data.data });
        }, err => {
            // console.log(err, 'Error Axios Instagram');
        } );
    }

    renderPosts = () => {
        const {posts} = this.state;

        if(!posts.length){
            return;
        }

        const instagramPosts = posts.map( (post, key) => {
            const link = post.link;
            const image = post.images.standard_resolution.url;
            return <a href={link} className="Instagram-single" style={{ backgroundImage: `url(${image})` }} key={key} target="_blank" rel="noopener noreferrer"></a>;
        } );

        return instagramPosts;
    }

    render(){
        return(
            <section className={`Instagram ${this.props.marginModifier}`}>
                <FontAwesomeIcon icon={['fab', 'instagram']} className="Instagram-icon" />
                <div className="Instagram-container">
                    {this.renderPosts()}
                </div>
            </section>   
        );
    }
}

Instagram.propTypes = {
    marginModifier: PropTypes.string
};