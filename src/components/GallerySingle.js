import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function GallerySingle(props){
    return(
        <div className="GallerySingle">
            <div className="GallerySingle-image" style={{ backgroundImage: `url(${props.image}` }}></div>
            <p className="GallerySingle-text">{props.text}</p>
            <Link to="/galeria" className="Button GallerySingle-button">{props.button}</Link>
        </div>
    );
}

GallerySingle.propTypes = {
    image: PropTypes.string,
    text: PropTypes.string,
    button: PropTypes.string
};