import React, { Component, Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import logo from '../img/menu/logo.png';
import envelope from '../img/contact_icon.png';
import menuIcon from '../img/menu_icon.png';
import closeIcon from '../img/close_icon.png';

export default class Menu extends Component {
    constructor(props){
        super(props);

        this.state = { menu: false };
    }

    onContactButtonClick = e => {
        const element = e.target;
        const targetName = element.dataset.scrollto;
        const targetElement = document.querySelector(`[data-scrolltarget=${targetName}]`);
        const targetOffset = targetElement.offsetTop;
        const menuHeight = document.querySelector('.Menu').offsetHeight;

        window.scroll({
            top: targetOffset - menuHeight,
            left: 0,
            behavior: 'smooth'
        });
    }

    onResponsiveMenuToggle = () => {
        const newMenuState = !this.state.menu;
        const menu = document.querySelector('.MenuResponsive');

        this.setState({ menu: newMenuState });

        menu.classList.toggle('MenuResponsive--open');
    }

    render(){
        return(
            <Fragment>
                <header className="Menu">
                    <Link to="/" className="Menu-left">
                        <img src={logo} alt="WiseLiving Logo"/>
                    </Link>
                    <div className="Menu-right">
                        <nav className="Menu-nav">
                            <ul className="Menu-nav-list">
                                <li className="Menu-nav-option"><NavLink exact to="/" activeClassName="Menu-nav-option--current">Inicio</NavLink></li>
                                <li className="Menu-nav-option"><NavLink exact to="/departamentos" activeClassName="Menu-nav-option--current">Departamentos</NavLink></li>
                                <li className="Menu-nav-option"><NavLink exact to="/proyecto" activeClassName="Menu-nav-option--current">Proyecto</NavLink></li>
                                <li className="Menu-nav-option"><NavLink exact to="/torres" activeClassName="Menu-nav-option--current">Torres</NavLink></li>
                                <li className="Menu-nav-option"><NavLink exact to="/galeria" activeClassName="Menu-nav-option--current">Galería</NavLink></li>
                                <li className="Menu-nav-option"><NavLink exact to="/ubicacion" activeClassName="Menu-nav-option--current">Ubicación</NavLink></li>
                                <li className="Menu-nav-option Menu-nav-option--contact"><NavLink exact to="/contacto" activeClassName="Menu-nav-option--current">Contacto</NavLink></li>
                            </ul>
                        </nav>
                    </div>
                    <ul className="Menu-right--responsive">
                        <li className="Menu-right--responsive-item" onClick={this.onContactButtonClick} data-scrollto="contact">
                            <div className="Menu-right--responsive-envelope" style={{ backgroundImage: `url(${envelope})` }}></div>
                        </li>
                        <li className="Menu-right--responsive-item" onClick={this.onResponsiveMenuToggle}>
                            <div className="Menu-right--responsive-menu" style={{ backgroundImage: this.state.menu ? `url(${closeIcon})` : `url(${menuIcon})`}}></div>
                        </li>
                    </ul>
                </header>
                <nav className="MenuResponsive">
                    <ul className="MenuResponsive-options">
                        <li className="MenuResponsive-option"><Link to="/">Inicio</Link></li>
                        <li className="MenuResponsive-option"><Link to="/departamentos">Departamentos</Link></li>
                        <li className="MenuResponsive-option"><Link to="/proyecto">Proyecto</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/proyecto?desarrolladores">Desarrolladores</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/proyecto?caracteristicas">Características</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/proyecto?amenidades">Amenidades</Link></li>
                        <li className="MenuResponsive-option"><Link to="/torres">Torres</Link></li>
                        <li className="MenuResponsive-option"><Link to="/galeria">Galería</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/galeria?interior">Interior</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/galeria?exterior">Exterior</Link></li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle"><Link to="/galeria?amenidades">Amenidades</Link></li>
                        <li className="MenuResponsive-option"><Link to="/ubicacion">Ubicación</Link></li>
                        <li className="MenuResponsive-option" onClick={this.onContactButtonClick} data-scrollto="contact">Contacto</li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle">
                            <div className="MenuResponsive-social">
                                <a href="https://www.facebook.com/wiselivingcanada/" target="_blank" rel="noopener noreferrer">
                                    <FontAwesomeIcon icon={['fab', 'facebook-f']} className="MenuResponsive-social-single" />
                                </a>
                                <a href="https://www.instagram.com/wiselivingcanada/" target="_blank" rel="noopener noreferrer">
                                    <FontAwesomeIcon icon={['fab', 'instagram']} className="MenuResponsive-social-single" />
                                </a>
                                <a href="tel:+524422946060" target="_blank" rel="noopener noreferrer">
                                    <FontAwesomeIcon icon="phone" className="MenuResponsive-social-single" />
                                </a>
                            </div>
                        </li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle">Aviso de privacidad</li>
                        <li className="MenuResponsive-option MenuResponsive-option--subtitle">Preguntas frecuentes</li>
                    </ul>
                </nav>
            </Fragment>
        );
    }
}