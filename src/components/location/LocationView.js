import React, { Fragment } from 'react';

import Menu from '../Menu';
import Welcome from '../Welcome';
import Location from './Location';
import Map from '../Map';
import Instagram from '../Instagram';
import Contact from '../Contact';
import Footer from '../Footer';

import welcomeBg from '../../img/home/welcome.jpg';

export default function LocationView(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} text="Una excelente ubicación en Juriquilla, Querétaro" />
            <Location />
            <Map />
            <Instagram marginModifier="Instagram--extraMarginTop" />
            <Contact />
            <Footer />
        </Fragment>
    );
}