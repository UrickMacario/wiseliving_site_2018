import React from 'react';

export default function LocationIntro(){
    return(
        <section className="LocationIntro" data-scrolltarget="welcomenext">
            <h2 className="LocationIntro-title">La mejor ubicación: Juriquilla, Querétaro.</h2>
            <p className="LocationIntro-text">Con una ubicación privilegiada a tan sólo 20 minutos de la ciudad de Querétaro, en la exclusiva zona de Juriquilla, este desarrollo residencial asegura la plusvalía de inversión con una infraestructura urbana sólida que integra funcionalidad, comodidad y facilidad de acceso.</p>
        </section>
    );
}