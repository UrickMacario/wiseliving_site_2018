import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

function TowersSingle(props){

    function renderButtons(){
        const location = props.location.pathname;
        const apartmentKey = props.tower;

        switch(location){
            case '/torres':
            return <button className="Button TowersSingle-button" data-apartment={apartmentKey} onClick={props.onTowerSingleDetails}>Ver Detalles</button>;

            default:
            return <Link to="/torres?scroll=towers" className="Button TowersSingle-button">{props.buttonText}</Link>;
        }
    }

    return(
        <div className="TowersSingle">
            <div className="TowersSingle-image" style={{ backgroundImage: `url(${props.image})` }}></div>
            <h3 className="TowersSingle-title">{props.title}</h3>
            <p className="TowersSingle-text">{props.text}</p>
            {renderButtons()}
        </div>
    );
}

TowersSingle.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    location: PropTypes.object,
    pathname: PropTypes.string,
    tower: PropTypes.number,
    onTowerSingleDetails: PropTypes.func,
    buttonText: PropTypes.string
};

const TowersSingleWithRouter = withRouter(TowersSingle);

export default TowersSingleWithRouter;