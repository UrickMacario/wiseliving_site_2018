import React, { Fragment } from 'react';

import Menu from '../Menu';
import PrivacyText from './PrivacyText';
import Footer from '../Footer';

export default function PrivacyView(){
  return(
    <Fragment>
      <Menu />
      <PrivacyText />
      <Footer />
    </Fragment>
  );
}