import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import SlideShow from './Slideshow';

let delay;

class Amenities extends Component{

    componentDidMount(){
        const params = this.props.location.search;

        if(!params){
            return;
        }

        this.waitNScroll(params);
    }

    waitNScroll = section => {
        let target, targetOffset;
        const menuHeight = document.querySelector('.Menu').offsetHeight;

        switch(section){
            case '?amenidades':
            target = document.querySelector('.Amenities');
            targetOffset = target.offsetTop;

            delay = setTimeout( () => {
                window.scroll({
                    top: targetOffset - menuHeight,
                    left: 0,
                    behavior: 'smooth'
                });
            }, 500 );

            break;
        }
    }

    componentWillUnmount(){
        clearTimeout(delay);
    }

    renderButton = () => {
        const currentLocation = this.props.location.pathname;

        switch(currentLocation){
            case '/proyecto':
            case '/torres':
            return <button className="Button Amenities-button">Descarga el brochure</button>;

            default:
            return <Link to="/galeria" className="Button Amenities-button">Descubre el confort de Wise Living</Link>;
        }
    }

    render(){
        return(
            <section className={`Amenities ${this.props.bg}`}>
                <div className="Amenities-container">
                    <h2 className="Amenities-title">Nuestras amenidades</h2>
                    <p className="Amenities-text">Un estilo de vida integral para nuestros usuarios. Disfruta de estas amenidades exclusivas:</p>
                    <div className="Amenities-list">
                        <ul className="Amenities-list-group">
                            <li className="Amenities-list-option">Gimnasio equipado</li>
                            <li className="Amenities-list-option">Baño con vapor</li>
                            <li className="Amenities-list-option">Cancha de pádel</li>
                            <li className="Amenities-list-option">Juegos infantiles</li>
                            <li className="Amenities-list-option">Asadores</li>
                        </ul>
                        <ul className="Amenities-list-group">
                            <li className="Amenities-list-option">Alberca semiolímpica con chapoteadero</li>
                            <li className="Amenities-list-option">Salón de proyecciones</li>
                            <li className="Amenities-list-option">Bar / cafetería</li>
                            <li className="Amenities-list-option">Salón de usos múltiples</li>
                            <li className="Amenities-list-option">Pet garden</li>
                        </ul>
                        <ul className="Amenities-list-group">
                            <li className="Amenities-list-option">Estacionamiento con espacio autónomo para salir o entrar libremente.</li>
                            <li className="Amenities-list-option">Dos elevadores con capacidad para 8 personas y un elevador de carga hasta 800 kgs.</li>
                            <li className="Amenities-list-option">Puertas eléctricas con accesos y salidas independientes para residentes y visitantes.</li>
                        </ul>
                    </div>
                </div>
                <SlideShow />
                {this.renderButton()}
            </section>
        );
    }

}

Amenities.propTypes = {
    bg: PropTypes.string,
    location: PropTypes.object,
    pathname: PropTypes.string
};

const AmenitiesWithRouter = withRouter(Amenities);

export default AmenitiesWithRouter;