import React, { Fragment } from 'react';

import Menu from './Menu';
import Welcome from './Welcome';
import Location from './Location';
import Towers from './Towers';
import Amenities from './Amenities';
import Characteristics from './Characteristics';
import Gallery from './Gallery';
import Instagram from './Instagram';
import Contact from './Contact';
import Footer from './Footer';

import welcomeBg from '../img/home/wiseliving_1.jpg';
import welcomeBg2 from '../img/home/wiseliving_2-squashed.jpg';
import welcomeBg3 from '../img/home/wiseliving_3.jpg';
import welcomeBg4 from '../img/home/wiseliving_4-squashed.jpg';
import welcomeBg5 from '../img/home/wiseliving_5-squashed.jpg';

const slides = [welcomeBg, welcomeBg2, welcomeBg3, welcomeBg4, welcomeBg5];

export default function Home(){
    return(
        <Fragment>
            <Menu />
            <Welcome image={welcomeBg} slides={slides} text="Exclusivos departamentos en Juriquilla: vanguardia y comodidad." />
            <Location />
            <Towers />
            <Amenities />
            <Characteristics />
            <Gallery />
            <Instagram />
            <Contact />
            <Footer />
        </Fragment>
    );
}